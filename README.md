this repo contains code for Katalon Studio, a free tool for test automation creation and management.

Katalon Docs - https://docs.katalon.com/katalon-studio/docs/index.html

Contents:

Object Repository - this contains each 'object' in the FastScout-web application, meaning every button or element that needs to be clicked on or used for verification in the UI tests. Objects are defined by either 'id' property or an xpath.

Test Cases - this contains the scripts describing the click paths and validation steps to execute, using the objects as references on what to 'click' or 'verify'. Katalon uses built-in keywords like 'click' or 'send keys' to make it easy to build workflows once your objects (buttons) are defined.

Test Suites - combinations of test-cases such as 'Consumer' and 'Regression Tests'. When these are executed, the results display in the Katalon TestOps web tool

Profiles - I have used these to define specific combinations of environment (localhost, staging, production) along
 with user login/league (NCAAB, WNCAAB, NBA, WNBA, EURO-M, etc.). These contains any variables that are used in the test-cases (like URL, username/password, primaryColor, etc.)



How to Create Web-UI Tests:

1. Download Katalon Studio (IDE for this test automation) desktop application: https://www.katalon.com/

2. Create new project or download this repo and open this project

3. Think through the 'click path' of the workflow you want to automate for your test case

4. Create Objects in the Object Repository for each button you click on or object you want to verify. Many objects are likely already created, so browse the Object Repository and use the search function in Katalon Studio. You can use the 'Record' or 'Spy' features in Katalon to grab these objects or insect them yourself using browser dev tools

5. Create a new test case, adding steps to 'click' (use 'Enhanced Click' keyword for best clicking results) each object in your workflow. 

6. Add steps to 'wait' or 'verify' as needed.

7. Click Play within Katalon Studio to execute your test-case, either in UI mode where you can view the execution live on your machine or in Headless mode.