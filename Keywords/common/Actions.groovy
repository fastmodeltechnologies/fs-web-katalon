package common
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


public class Actions {
	@Keyword
	def signInUser1() {
		WebUI.openBrowser('https://fastscout-staging.fastmodelsports.com/')

		WebUI.setText(findTestObject('Sign In Page/Email Field'), 'qaautomation1@staging.com')

		WebUI.setEncryptedText(findTestObject('Sign In Page/Password Field'), 'YzGUhxdT33ALtOhrSF0KEQ==')

		WebUI.click(findTestObject('Sign In Page/Sign In Button'))

		WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)
	}

	@Keyword
	def signInUser1Local() {
		WebUI.openBrowser('')

		WebUI.navigateToUrl('http://localhost:3000')

		WebUI.setText(findTestObject('Sign In Page/Email Field'), 'qaautomation1@staging.com')

		WebUI.setEncryptedText(findTestObject('Sign In Page/Password Field'), 'YzGUhxdT33ALtOhrSF0KEQ==')

		WebUI.click(findTestObject('Sign In Page/Sign In Button'))

		WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)
	}
	@Keyword
	def signInUserScout() {
		WebUI.openBrowser('')

		WebUI.navigateToUrl('https://fastscout-staging.fastmodelsports.com/')

		WebUI.setText(findTestObject('Sign In Page/Email Field'), 'autoscout@fastmodelqa.com')

		WebUI.setEncryptedText(findTestObject('Sign In Page/Password Field'), 'YzGUhxdT33ALtOhrSF0KEQ==')

		WebUI.click(findTestObject('Sign In Page/Sign In Button'))

		WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)
	}
	@Keyword
	def signInUserScoutLocal() {
		WebUI.openBrowser('')

		WebUI.navigateToUrl('http://localhost:3000')

		WebUI.setText(findTestObject('Sign In Page/Email Field'), 'autoscout@fastmodelqa.com')

		WebUI.setEncryptedText(findTestObject('Sign In Page/Password Field'), 'YzGUhxdT33ALtOhrSF0KEQ==')

		WebUI.click(findTestObject('Sign In Page/Sign In Button'))

		WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)
	}

	@Keyword
	def createOpponentScoutBlankTemplate() {
		WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

		WebUI.click(findTestObject('Scouts - Opponent Scouts/NEW SCOUTING REPORT button'))

		WebUI.click(findTestObject('Objects I know I have to replace/Opponent Team dropdown (needs ID update)'))

		WebUI.click(findTestObject('New Scout modal/1st team in Opponent Team dropdown'))

		WebUI.click(findTestObject('New Scout modal/Blank Template'))

		WebUI.click(findTestObject('New Scout modal/CREATE button'))
	}

	@Keyword
	def archiveOpponentScout() {
		WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

		WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Context Menu (Opponent Scouts)'))

		WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))
	}


	@Keyword
	def deleteScout() {
		WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

		WebUI.click(findTestObject('Scouts - Archived/Scout Archive Menu'))

		WebUI.click(findTestObject('Scouts - Archived/Delete button'))

		WebUI.click(findTestObject('Scouts - Archived/Confirm Delete'))
	}


	@Keyword
	def addSectionHeaderTile() {
		WebUI.click(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header Tile'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

		WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))
	}

	@Keyword
	def editSectionHeaderTile() {
		WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))
	}

	@Keyword
	def deleteSectionHeaderTile() {
		WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))
	}

	@Keyword
	def addFastScoutFactorsTile() {
		WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

		WebUI.click(findTestObject('ScoutBuilder/Tiles/FastScout Factors Tile (Folder)/FastScout Factors Tile'))

		WebUI.delay(1)

		WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

		WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))
	}
}