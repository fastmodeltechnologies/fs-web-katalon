import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 0)

WebUI.enhancedClick(findTestObject('Top Nav/SCOUTS tab'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Object Repository/New Scout modal/Blank Template'))

WebUI.delay(1)

WebUI.setText(findTestObject('New Scout modal/SCOUTING REPORT NAME field'), 'Our Rivals')

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header Tile'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Spacer Tile (Folder)/Spacer Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Custom Player Input'))

WebUI.setText(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Custom Player Input'), 'Malik Taylor')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Add Custom Player'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Custom Player1 Checkbox'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/2x2 in tile builder'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/2x2 in tile builder'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'))

WebUI.delay(3)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.waitForElementClickable(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'), 0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('SCOUTS sub-nav/OPPONENT SCOUTS tab'), 0)

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/OPPONENT SCOUTS tab'))

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scount Context Menu (Opponent Scouts) - Consumer'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Scout Archive Menu - Consumer'))

WebUI.waitForElementClickable(findTestObject('Scouts - Archived/Delete button'), 0)

WebUI.mouseOver(findTestObject('Scouts - Archived/Delete button'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Delete button'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

