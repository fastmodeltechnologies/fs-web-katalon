import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.emailscout)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.passwordscout)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('Team Page/Roster (Folder)/Roster tab'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Top Nav/Season Dropdown'))

WebUI.enhancedClick(findTestObject('Top Nav/Season Dropdown (last season)'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('Player Page (Folder)/1st player on Roster page'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Player Page (Folder)/Game Log tab'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Player Page (Folder)/Career tab'))

WebUI.delay(4)

WebUI.enhancedClick(findTestObject('Player Page (Folder)/Stats tab (Player Page)'))

WebUI.enhancedClick(findTestObject('Player Page (Folder)/New Player Scout (player page)'))

WebUI.enhancedClick(findTestObject('New Scout modal/Blank Template'))

WebUI.enhancedClick(findTestObject('New Scout modal/SCOUTING REPORT NAME field'))

WebUI.sendKeys(findTestObject('New Scout modal/SCOUTING REPORT NAME field'), 'TestPlayerScout')

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.delay(4)

WebUI.waitForElementClickable(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'), 0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.enhancedClick(findTestObject('Top Nav/SCOUTS tab'))

WebUI.mouseOver(findTestObject('Scouts - Player Scouts/Player Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Scout Context Menu (Player Scouts)'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/ARCHIVED (scout role)'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.waitForElementClickable(findTestObject('Scouts - Archived/Delete button'), 0)

WebUI.mouseOver(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

