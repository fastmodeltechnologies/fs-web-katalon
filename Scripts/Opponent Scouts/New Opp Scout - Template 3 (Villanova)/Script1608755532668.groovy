import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.click(findTestObject('New Scout modal/Opponent Team'))

WebUI.click(findTestObject('New Scout modal/1st team in Opponent Team dropdown'))

WebUI.enhancedClick(findTestObject('New Scout modal/Season Dropdown (New Scout)'))

WebUI.enhancedClick(findTestObject('New Scout modal/Last Season (New Scout)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('New Scout modal/Saved Template Dropdown'))

WebUI.sendKeys(findTestObject('New Scout modal/Saved Template Dropdown Field'), '3')

WebUI.sendKeys(findTestObject('New Scout modal/Saved Template Dropdown Field'), Keys.chord(Keys.ENTER))

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.click(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Context Menu (Opponent Scouts)'))

WebUI.waitForElementClickable(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'), 0)

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.click(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.waitForElementClickable(findTestObject('Scouts - Archived/Delete button'), 0)

WebUI.mouseOver(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

