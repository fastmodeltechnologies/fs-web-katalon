import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.enhancedClick(findTestObject('Top Nav/SCOUTS tab'))

WebUI.waitForElementClickable(findTestObject('NEW SCOUT button'), 0)

WebUI.mouseOver(findTestObject('NEW SCOUT button'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.enhancedClick(findTestObject('New Scout modal/Opponent Team'))

WebUI.enhancedClick(findTestObject('New Scout modal/1st team in Opponent Team dropdown'))

WebUI.enhancedClick(findTestObject('New Scout modal/Blank Template'))

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Size (3)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/TO Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PACE'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Minimum Minutes'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Minimum Minutes'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Minimum Minutes'), '30')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Lineups Displayed (Count)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Lineups Displayed (Count)'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Lineups Displayed (Count)'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Lineups Displayed (Count)'), '5')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Home Split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/USG Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/REB'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Minimum FGA Field (Top Scorers)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Minimum FGA Field (Top Scorers)'), 
    '5')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3 Point Shooters Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'), '3')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/3PM-A Column Order'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/Minimum 3PA Field (3PT Shooters)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/Minimum 3PA Field (3PT Shooters)'), 
    '5')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3P-R (3PT Shooters Tile)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/Minimum FTA Field (FT Shooters)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/Minimum FTA Field (FT Shooters)'), 
    '2')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Sort By FT Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Sort By FT Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Rebounds (Folder)/Rebounds Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Last 5 split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Ball Control Tile (Folder)/Ball Control Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Defense Tile (Folder)/Defense Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Defense Tile (Folder)/BLK column header (on tile)'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Recent Games Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'), '8')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Recent Games DATE column'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/Depth Chart Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/First Name (Depth Chart)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/First Initial'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/2x2 in tile builder'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Team Shot Chart Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Zone Shot Chart Type'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Shot Chart Show FG Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 2'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Make-Miss Shot Chart Type'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Last 5 split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 2'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu (Opponent Scouts)'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Delete button'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

