import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.enhancedClick(findTestObject('Top Nav/SCOUTS tab'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.click(findTestObject('New Scout modal/Opponent Team'))

WebUI.click(findTestObject('New Scout modal/1st team in Opponent Team dropdown'))

WebUI.enhancedClick(findTestObject('New Scout modal/Blank Template'))

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.delay(6)

WebUI.waitForElementVisible(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'), 0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header background color (1)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Spacer Tile (Folder)/Spacer Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'), 
    0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Add Videos'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video Object'), '/Users/jefffeldman/Desktop/Vids/Press.mp4')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Object'), '/Users/jefffeldman/Desktop/testimage.png')

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Uploaded Image (Presence Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Add Videos'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/CANCEL button (Tile Builder)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/FastScout Factors Tile (Folder)/FastScout Factors Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/League Rankings button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/FTRate'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/AST Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/FastScout Factors Chart Tile (Folder)/FastScout Factors Chart Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Home split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/FastScout Factors Chart Tile (Folder)/FF Chart Dropdown'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/FastScout Factors Chart Tile (Folder)/FF Chart Dropdown TrueShootingPercentage'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Points Per Period Tile (Folder)/Points Per Period Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('Objects I know I have to replace/Team Dropdown (Tile Editor)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/My Team (Team Dropdown) Tile Editor'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Points Per Period Tile (Folder)/PPP Overtime Checkbox'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.delay(1)

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.delay(1)

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Home split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/League Rankings button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Pace Chart Tile (Folder)/Team Pace Chart Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Home split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Pace Chart Tile (Folder)/Opponent Conference checkbox (Team Pace Chart)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Individual Boxscore Tile (folder)/Individual Boxscore Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Individual Boxscore Tile (folder)/Individual Boxscore Game 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/USG Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus-Minus'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/REB'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Cumulative Boxscore Tile (folder)/Cumulative Boxscore Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/First listed game'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/eFG Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/3P-R'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/FTM-A'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/STL'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/USG Perct'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PTS Column Order'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stat Comparison Tile (Folder)/Team Stat Comparison Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/ADD VIDEO button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/From Synergy'))

WebUI.setText(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Username'), 'sachin@fastmodeltechnologies.com')

WebUI.setEncryptedText(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Password'), 'YzGUhxdT33BEbO8XdJTy9g==')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Login button'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Edit-Clip Search'))

WebUI.setText(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Edit-Clip Search'), 'Toronto')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/First Synergy Edit'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/First Synergy Clip'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/DONE button in Synergy Clips modal'))

WebUI.enhancedClick(findTestObject('VIDEOS page (Folder)/ADD CLIPS button in Synergy Edits modal'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Video (Presence Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/Team Stat Splits Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/All Games offensive split (Team Stat Splits tile)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/All Games defensive split (Team Stat Splits tile)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Custom Split (Personnel,TeamStatSplits)/Add Custom Split'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Custom Split (Personnel,TeamStatSplits)/All button in NEW CUSTOM SPLIT modal'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Custom Split (Personnel,TeamStatSplits)/SPLIT NAME text box (new custom split)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/GAMES tab/Custom Split (Personnel,TeamStatSplits)/SPLIT NAME text box (new custom split)'), 
    'MySplit')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/GAMES tab/Custom Split (Personnel,TeamStatSplits)/ADD new custom split button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PTS'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PACE (on Team Stat Splits)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Whole Numbers'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Advanced Stats Tile (Folder)/Team Advanced Stats Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile GAMES tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PW'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/PL'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/League Rankings button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Settings'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'), '3')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'), '3')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Custom Stat Text Field'), 'myStat')

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus button to add custom stat'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile PLAYERS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Tile SAMPLE tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Tile VIDEOS tab'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Tile Ellipsis Button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/TIle Ellipsis Menu/Delete Tile'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu (Opponent Scouts)'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Delete button'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

