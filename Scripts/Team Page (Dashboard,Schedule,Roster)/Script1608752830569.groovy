import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('Sign In Page/Sign In Button'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Splits (dashboard folder)/LAST 5 (Dashboard)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Splits (dashboard folder)/Wins vs Losses (Dashboard)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Splits (dashboard folder)/Home vs Away (Dashboard)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Splits (dashboard folder)/All Games (Dashboard)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart button (Dashboard)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart TS Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart TO Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart AST Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart OR Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart DR Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart FTRATE (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/FF Chart FT Perct (DASHBOARD)'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Chart Object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/FastScout Factors (folder)/Fastscout Factor Table button (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Pace (dashboard folder)/Pace Chart button (Dashboard)'))

WebUI.verifyElementVisible(findTestObject('Team Page/Dashboard (Folder)/Pace (dashboard folder)/Pace Chart object (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Pace (dashboard folder)/Pace Table button (Dashboard)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Ranking dropdown (DASHBOARD)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Ranking Dropdown NONE (DASHBOARD)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Ranking dropdown (DASHBOARD)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Ranking Dropdown LEAGUE (DASHBOARD)'))

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/TOTALS boxscore button'))

WebUI.delay(1)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/ADVANCED boxscore button'))

WebUI.delay(1)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/PER 40 boxscore button'))

WebUI.delay(1)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/AVERAGES boxscore button'))

WebUI.click(findTestObject('Top Nav/Season Dropdown'))

WebUI.click(findTestObject('Top Nav/Season Dropdown (last season)'))

WebUI.delay(2)

WebUI.click(findTestObject('Top Nav/Season Dropdown'))

WebUI.click(findTestObject('Top Nav/Season Dropdown (current season)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Schedule (Folder)/SCHEDULE tab'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/LAST 5 button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/WINS button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/LOSSES button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/HOME button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/AWAY button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/vs Team dropdown (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/vs Team dropdown option 1'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/ALL GAMES button (SCHEDULE)'))

WebUI.enhancedClick(findTestObject('Top Nav/Season Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Top Nav/Season Dropdown (last season)'))

WebUI.delay(2)

WebUI.click(findTestObject('Top Nav/Season Dropdown'))

WebUI.click(findTestObject('Top Nav/Season Dropdown (current season)'))

WebUI.delay(2)

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Splits (schedule folder)/LAST 5 button (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Select All (SCHEDULE)'))

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Print icon (SCHEDULE)'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Team Page/Schedule (Folder)/Cancel (print modal)'), 0)

WebUI.click(findTestObject('Team Page/Schedule (Folder)/Cancel (print modal)'))

WebUI.click(findTestObject('Team Page/Roster (Folder)/Roster tab'))

WebUI.delay(8)

WebUI.waitForElementClickable(findTestObject('Team Page/Roster (Folder)/2 players per row'), 0)

WebUI.enhancedClick(findTestObject('Team Page/Roster (Folder)/2 players per row'))

WebUI.click(findTestObject('Team Page/Scouts (Team Subtab Folder)/Self Scouts tab (Team page)'))

WebUI.click(findTestObject('Top Nav/OPPONENTS tab'))

WebUI.click(findTestObject('Opponents page/SCHEDULED button'))

WebUI.click(findTestObject('Opponents page/ALL TEAMS button'))

WebUI.click(findTestObject('Opponents page/SCHEDULED button'))

WebUI.click(findTestObject('Top Nav/PLAYS tab'))

WebUI.delay(2)

WebUI.click(findTestObject('Top Nav/VIDEOS tab'))

WebUI.closeBrowser()

