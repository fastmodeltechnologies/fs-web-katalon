import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.emailscout)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.passwordscout)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.enhancedClick(findTestObject('Top Nav/SCOUTS tab'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.enhancedClick(findTestObject('New Scout modal/Saved Template Dropdown Field'))

WebUI.sendKeys(findTestObject('New Scout modal/Saved Template Dropdown Field'), '7')

WebUI.sendKeys(findTestObject('New Scout modal/Saved Template Dropdown Field'), Keys.chord(Keys.ENTER))

WebUI.enhancedClick(findTestObject('New Scout modal/SCOUTING REPORT NAME field'))

WebUI.sendKeys(findTestObject('New Scout modal/SCOUTING REPORT NAME field'), 'TestPlayerScout')

WebUI.enhancedClick(findTestObject('New Scout modal/CREATE button'))

WebUI.delay(4)

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/AddPlayer1'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/Select a Player'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutLeagueDropdown'), 'D')

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutLeagueDropdown'), Keys.chord(
        Keys.ENTER))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutTeamDropdown'))

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutTeamDropdown'), 'indiana')

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutTeamDropdown'), Keys.chord(
        Keys.ENTER))

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'))

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'), Keys.chord(
        Keys.DOWN))

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'), Keys.chord(
        Keys.ENTER))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Object'), '/Users/jefffeldman/Desktop/testimage.png')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Uploaded Image (Presence Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Pagination/Page 2'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Select Player (PS1)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Select Player Radio button (PlayerShotChartTile)'))

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'))

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'), Keys.chord(
        Keys.DOWN))

WebUI.sendKeys(findTestObject('Scouts - Player Scouts/Player Scout Personnel Tiles/playerScoutPlayerDropdown'), Keys.chord(
        Keys.ENTER))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/Presenter Mode/Presenter Mode (ScoutBuilder)'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Root'), Keys.chord(Keys.TAB))

WebUI.sendKeys(findTestObject('Root'), Keys.chord(Keys.TAB))
WebUI.sendKeys(findTestObject('Root'), Keys.chord(Keys.TAB))

WebUI.sendKeys(findTestObject('Root'), Keys.chord(Keys.TAB))

WebUI.sendKeys(findTestObject('Root'), Keys.chord(Keys.TAB))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/Presenter Mode/Exit Presenter Mode'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'), 0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.mouseOver(findTestObject('Scouts - Player Scouts/Player Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Player Scouts/Scout Context Menu (Player Scouts)'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/ARCHIVED (scout role)'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.waitForElementClickable(findTestObject('Scouts - Archived/Delete button'), 0)

WebUI.mouseOver(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.closeBrowser()

