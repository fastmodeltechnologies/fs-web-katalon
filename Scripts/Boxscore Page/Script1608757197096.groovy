import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.enhancedClick(findTestObject('Top Nav/OPPONENTS tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Opponents page/SCHEDULED button'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('Boxscore Page/SCHEDULED boxscore1'))

WebUI.delay(20)

WebUI.enhancedClick(findTestObject('Boxscore Page/PLAY BY PLAY tab'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('Boxscore Page/BOXSCORE tab'))

WebUI.delay(20)

WebUI.enhancedClick(findTestObject('Boxscore Page/Back to Calendar'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Top Nav/OPPONENTS tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Opponents page/ALL TEAMS button'))

WebUI.delay(1)

WebUI.enhancedClick(GlobalVariable.allTeams1)

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Team Page/Schedule (Folder)/SCHEDULE tab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Top Nav/Season Dropdown'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Top Nav/Season Dropdown (last season)'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Boxscore Page/SCHEDULED boxscore1'))

WebUI.delay(20)

WebUI.enhancedClick(findTestObject('Boxscore Page/PLAY BY PLAY tab'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Boxscore Page/BOXSCORE tab'))

WebUI.delay(20)

WebUI.enhancedClick(findTestObject('Boxscore Page/Back to Calendar'))

WebUI.delay(2)

WebUI.closeBrowser()

