import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email2)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password2)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.verifyElementNotPresent(findTestObject('Team Page/Dashboard (Folder)/Advanced Stats table (Dashboard)'), 0)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/ADVANCED boxscore button'))

WebUI.verifyElementNotPresent(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/PlusMinus column header'), 
    0)

WebUI.click(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/PER 40 boxscore button'))

WebUI.verifyElementNotPresent(findTestObject('Team Page/Dashboard (Folder)/Boxscore (dashboard folder)/Per 40 boxscore button (not available)'), 
    0)

WebUI.click(findTestObject('Top Nav/PLAYS tab'))

WebUI.verifyElementNotPresent(findTestObject('Plays Page (folder)/Plays tab'), 0)

WebUI.click(findTestObject('Top Nav/VIDEOS tab'))

WebUI.click(findTestObject('VIDEOS page (Folder)/Add Video button (Videos Page)'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/From Synergy'), 0)

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video Object'), '/Users/jefffeldman/Desktop/Vids/Press.mp4')

WebUI.verifyElementVisible(findTestObject('VIDEOS page (Folder)/First Row on VIDEOS page'))

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.verifyElementNotPresent(findTestObject('Scouts - Player Scouts/Player Scouts tab'), 0)

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Context Menu (Opponent Scouts)'))

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Presenter Mode (Scout Context Menu)'), 
    0)

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Mobile Access (Scout Context Menu)'), 
    0)

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Mobile Usage'), 0)

WebUI.click(findTestObject('Scouts - Opponent Scouts/Open first opponent scout row'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Individual Boxscore Tile (folder)/Individual Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile OPTIONS Tab'))

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/USG Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Plus-Minus'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/eFG Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/TS Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/OREB Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/DREB Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/AST Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/TO Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/STL Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/BLK Perct'), 'data-enabled', 'false', 
    0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Cumulative Boxscore Tile (folder)/Cumulative Boxscore Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/CANCEL button (Tile Builder)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Advanced Stats Tile (Folder)/Team Advanced Stats Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Tile STATS tab'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3 Point Shooters Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Rebounds (Folder)/Rebounds Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Ball Control Tile (Folder)/Ball Control Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS_OPTIONS tab/Per 40 Stat Type'), 0)

