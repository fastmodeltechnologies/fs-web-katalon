import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Sign In Page/Email Field'), GlobalVariable.email)

WebUI.setText(findTestObject('Sign In Page/Password Field'), GlobalVariable.password)

WebUI.click(findTestObject('Sign In Page/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.waitForElementClickable(findTestObject('SCOUTS sub-nav/SELF SCOUTS tab'), 0)

WebUI.enhancedClick(findTestObject('SCOUTS sub-nav/SELF SCOUTS tab'))

WebUI.enhancedClick(findTestObject('NEW SCOUT button'))

WebUI.setText(findTestObject('New Scout modal/SCOUTING REPORT NAME field'), 'Self Scout Blank Template')

WebUI.click(findTestObject('New Scout modal/Blank Template'))

WebUI.click(findTestObject('New Scout modal/CREATE button'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header Tile'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Spacer Tile (Folder)/Spacer Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/FastScout Factors Chart Tile (Folder)/FastScout Factors Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 2'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Points Per Period Tile (Folder)/Points Per Period Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Points Per Period Tile (Folder)/Points Per Period Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Points Per Period Tile (Folder)/Points Per Period Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Team Pace Chart Tile (Folder)/Team Pace Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Pace Chart Tile (Folder)/Team Pace Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Individual Boxscore Tile (folder)/Individual Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Individual Boxscore Tile (folder)/Individual Boxscore Game 1'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 3'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Cumulative Boxscore Tile (folder)/Cumulative Boxscore Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Cumulative Boxscore Tile (folder)/Cumulative Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Cumulative Boxscore Tile (folder)/Cumulative Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Team Stat Comparison Tile (Folder)/Team Stat Comparison Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Team Stat Comparison Tile (Folder)/Team Stat Comparison Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stat Comparison Tile (Folder)/Team Stat Comparison Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 4'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats (Tile Menu)'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/Team Stat Splits Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/Team Stat Splits Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stat Splits Tile (Folder)/Team Stat Splits Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Team Advanced Stats Tile (Folder)/Team Advanced Stats Tile'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Clutch Stats Tile (Folder)/Clutch Stats Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 5'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Lineup Stats Tile (Folder)/Lineup Stats Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.verifyElementClickable(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Recent Games Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/Depth Chart Tile'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/2x2 in tile builder'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/2x2 in tile builder'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 6'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.enhancedClick(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Team Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'))

WebUI.delay(3)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/Template (Folder)/Create Template (ScoutBuilder)'))

WebUI.setText(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/Template (Folder)/New Template Name text input'), 'test template')

WebUI.click(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/Template (Folder)/Save new template button'))

WebUI.delay(4)

WebUI.click(findTestObject('ScoutBuilder/Top Menu (ScoutBuilder)/DONE button'))

WebUI.mouseOver(findTestObject('Scouts - Self Scouts/Self Scouts 1st Row'))

WebUI.enhancedClick(findTestObject('Scouts - Self Scouts/Scout Context Menu (Self Scouts)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Mobile Access (Scout Context Menu)'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Mobile Access modal/Mobile Access Select All'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Mobile Access modal/Mobile Access Save button'))

WebUI.delay(2)

WebUI.verifyElementAttributeValue(findTestObject('Scouts - Self Scouts/Self Scouts Mobile Access Glyph'), 'color', GlobalVariable.primaryColor, 
    0)

WebUI.mouseOver(findTestObject('Scouts - Self Scouts/Self Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Self Scouts/Scout Context Menu (Self Scouts)'))

WebUI.enhancedClick(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Archive'))

WebUI.click(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

WebUI.mouseOver(findTestObject('Scouts - Archived/Archived Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Archived/Scout Archive Menu'))

WebUI.waitForElementClickable(findTestObject('Scouts - Archived/Delete button'), 0)

WebUI.mouseOver(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Delete button'))

WebUI.click(findTestObject('Scouts - Archived/Confirm Delete Scout'))

WebUI.click(findTestObject('SCOUTS sub-nav/TEMPLATES tab'))

WebUI.verifyTextPresent('test template', false)

WebUI.mouseOver(findTestObject('Scouts - Templates/Scouts - Templates 1st row'))

WebUI.click(findTestObject('Scouts - Templates/Templates Context Menu'))

WebUI.enhancedClick(findTestObject('Scouts - Archived/Delete button'))

WebUI.delay(1)

WebUI.click(findTestObject('Scouts - Templates/Confirm Delete Template'))

WebUI.delay(1)

WebUI.verifyTextNotPresent('test template', false)

WebUI.closeBrowser()

