<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>FastScout Web Regression Tests</description>
   <name>Regression Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f75df59f-307f-407e-8e50-26c2c9121b13</testSuiteGuid>
   <testCaseLink>
      <guid>b5bb7991-4943-46d8-9184-1592c25b0da3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d9ffaef-9732-4f30-8003-5653d8b68235</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Self Scouts/New Self Scout - Blank Template - Add All Tiles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57efe487-92dd-4004-bd3d-65323e3de488</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Blank Template - Add-Edit-Delete All Tiles (Pt 1)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63357012-a9e4-4d95-8370-5f216f6db24f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Blank Template - Add-Edit-Delete All Tiles (Pt 2)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68afe965-04e6-461a-ad9e-16fb429169d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Team Page (Dashboard,Schedule,Roster)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c62e6453-3578-4d51-bd8c-56bffb0319df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Self Scouts/New Self Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3246797b-ed76-4eb9-873b-832078c8bab6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/New Player Scout - Saved Template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6784e6e3-7b8c-4e96-b280-4f0a0d21068b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Player Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0c262dd-e644-4399-95dc-923e5d15b348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ScoutBuilder Design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc5dbaf9-35f6-4f4e-8147-a54056e2e4f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Boxscore Page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>37fe22e7-4fed-4e40-887b-d4b9f3e9bfb1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
