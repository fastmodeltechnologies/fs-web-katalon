<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Consumer Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3b74ca66-ed46-44a6-9c01-3b7b462e3f5f</testSuiteGuid>
   <testCaseLink>
      <guid>457079f2-d87a-4920-b2f9-75158c55df1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Consumer/Consumer Opp Scout (Blank)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>feff745f-8974-4305-a953-5226d17c82ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Consumer/Consumer Opp Scout (Template)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
