<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Scout Templates</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>cba84d7f-b296-4c85-9b69-b2eecec59dd8</testSuiteGuid>
   <testCaseLink>
      <guid>24febd45-0aae-492e-9ba6-6df4f569d2c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 2 (My Team Personnel)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a00180b-345a-41d1-ac96-9720fd978738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 3 (Villanova)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bacbb12-3faf-4ec1-80c8-acba4a07beb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 4 (SIU-Edwardsville)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9b1405b-4041-4b32-8526-6e77768e5308</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 5 (Hornets)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ab910f2-c888-471a-b46b-79566557b7e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 6 (Duluth DII)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb300d4f-67b8-4acf-ad5b-c0ca4800aac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e19f1915-7ec2-4a28-bea5-b9a91bf25e22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/New Player Scout - Saved Template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2015926-3e3d-4c33-9b69-adec3817414e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Self Scouts/New Self Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
