<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>edc4b567-7c6c-4cbf-9f35-80db20c09b5d</testSuiteGuid>
   <testCaseLink>
      <guid>e5a86369-19f3-4ad9-8295-4774554ef082</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Player Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0849b7ba-728d-4b0c-9030-47713ddee323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Self Scouts/New Self Scout - Blank Template - Add All Tiles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d704574-b36e-4fe5-808d-3884a2497283</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Self Scouts/New Self Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9241e4e0-0229-45d5-8c2f-66d60d7d2347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ScoutBuilder Design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e365fe02-d054-4ba5-b9c6-4af1b238debb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 3 (Villanova) - Change Personnel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8050e170-d84f-4da1-b11e-44913e17e745</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 1 (1 of each tile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41300354-d5cc-4138-9c9d-1d8b444a4868</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Blank Template - Add-Edit-Delete All Tiles (Pt 1)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d113b5a9-4540-4cda-88e9-1a2afca1a682</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Blank Template - Add-Edit-Delete All Tiles (Pt 2)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5eb4822e-07ac-4837-b5ef-c299b8894de3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 6 (Duluth DII)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d7fa754-9011-4737-9f4b-2119f6125dcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 4 (SIU-Edwardsville)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6a610d7-f32c-458a-8e16-ca91ecce33c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 5 (Hornets)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5acfd839-20dd-4283-b17e-b24bfcbdafdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 3 (Villanova)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1397911d-af95-4aea-b73f-dc53a48c748c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Opponent Scouts/New Opp Scout - Template 2 (My Team Personnel)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e102570-6527-4eb0-bd2d-59e24cc580bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/New Player Scout - Saved Template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbf000c6-2f67-4fbc-becf-bf5dea187d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Features hidden behind feature flags</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62f04091-e705-4e64-9488-ad0d88997ad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Consumer/Consumer Opp Scout (Blank)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d07eb768-c5eb-4791-b26a-baf1f14417cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Consumer/Consumer Opp Scout (Template)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eabc4e6c-22e2-4160-8bb4-cb2eceef365a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Boxscore Page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>37fe22e7-4fed-4e40-887b-d4b9f3e9bfb1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d6d080c9-5dea-443c-b940-e77dafaca1e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Team Page (Dashboard,Schedule,Roster)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
