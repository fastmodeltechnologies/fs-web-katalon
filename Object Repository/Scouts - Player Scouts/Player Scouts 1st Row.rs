<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Top scout listed on SCOUTS>SELF SCOUTS page</description>
   <name>Player Scouts 1st Row</name>
   <tag></tag>
   <elementGuidId>f2010394-a358-4c03-a0da-90bcdb0dc755</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]</value>
   </webElementProperties>
</WebElementEntity>
