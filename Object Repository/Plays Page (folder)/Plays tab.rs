<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Plays sub-tab on PLAYS page</description>
   <name>Plays tab</name>
   <tag></tag>
   <elementGuidId>51ac6ba8-71cc-4455-9091-90cf8d401ab1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'plays-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>plays-0</value>
   </webElementProperties>
</WebElementEntity>
