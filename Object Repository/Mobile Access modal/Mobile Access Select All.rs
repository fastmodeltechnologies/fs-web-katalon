<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Select All checkbox in Mobile Access modal</description>
   <name>Mobile Access Select All</name>
   <tag></tag>
   <elementGuidId>04d4793f-8bfe-4615-8aee-be55f78b4b08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'mobile-access-select-all-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mobile-access-select-all-label</value>
   </webElementProperties>
</WebElementEntity>
