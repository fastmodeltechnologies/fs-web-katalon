<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>SAVE button in Mobile Access modal</description>
   <name>Mobile Access Save button</name>
   <tag></tag>
   <elementGuidId>eff21d03-2ad4-49e9-81cc-42ef21be2943</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'save-report-mobile-access']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>save-report-mobile-access</value>
   </webElementProperties>
</WebElementEntity>
