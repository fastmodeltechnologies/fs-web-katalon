<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Blank Scouting Report radio button</description>
   <name>Blank Template</name>
   <tag></tag>
   <elementGuidId>7409a548-08e1-447c-865e-2bfadf595630</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;blankTemplate&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;blankTemplate&quot;]</value>
   </webElementProperties>
</WebElementEntity>
