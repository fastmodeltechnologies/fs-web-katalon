<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First listed team in Opponent Team dropdown of New Opponent Scout modal</description>
   <name>1st team in Opponent Team dropdown</name>
   <tag></tag>
   <elementGuidId>df1c1d4b-74bf-4b1d-9370-b3339f113222</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'react-select-2--option-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-2--option-0</value>
   </webElementProperties>
</WebElementEntity>
