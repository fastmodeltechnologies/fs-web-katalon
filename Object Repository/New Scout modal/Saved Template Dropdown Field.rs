<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Saved Template Dropdown Field</name>
   <tag></tag>
   <elementGuidId>c1de566f-46c3-4654-a44a-f596cca92940</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'choosesavedTemplateDropdown']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>choosesavedTemplateDropdown</value>
   </webElementProperties>
</WebElementEntity>
