<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Scout Context Menu (Opponent Scouts)</name>
   <tag></tag>
   <elementGuidId>b16d3470-6525-4446-9e19-3376f20a9321</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[10]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[10]/div</value>
   </webElementProperties>
</WebElementEntity>
