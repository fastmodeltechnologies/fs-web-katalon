<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1st report listed on Opponent Scouts page</description>
   <name>Opponent Scouts 1st Row</name>
   <tag></tag>
   <elementGuidId>83000bc7-184d-4274-87e3-5cde1a4e3f80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]</value>
   </webElementProperties>
</WebElementEntity>
