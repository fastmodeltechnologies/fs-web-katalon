<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Mobile Access (Scout Context Menu)</name>
   <tag></tag>
   <elementGuidId>52b9fb0c-1224-43c7-9fb8-46a03dd75618</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'mobileAccess']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mobileAccess</value>
   </webElementProperties>
</WebElementEntity>
