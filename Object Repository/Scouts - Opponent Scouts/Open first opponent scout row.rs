<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open first opponent scout row</name>
   <tag></tag>
   <elementGuidId>3b31cd9e-8f53-44e0-8af3-9b627cf23624</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[3]</value>
   </webElementProperties>
</WebElementEntity>
