<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ALL TEAMS team1 (No Conferences)</name>
   <tag></tag>
   <elementGuidId>08140cd7-3bc5-4d90-98bd-ddc1559d82c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;container-team-name-column&quot;]/li[1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;container-team-name-column&quot;]/li[1]/a</value>
   </webElementProperties>
</WebElementEntity>
