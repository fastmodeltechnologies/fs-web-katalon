<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>First Row on VIDEOS page</name>
   <tag></tag>
   <elementGuidId>bf854cb5-d259-4d98-b532-264126583ff5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;VideoTable&quot;]/tbody/tr</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;VideoTable&quot;]/tbody/tr</value>
   </webElementProperties>
</WebElementEntity>
