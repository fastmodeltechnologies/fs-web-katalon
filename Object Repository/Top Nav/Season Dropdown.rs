<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Season Dropdown</name>
   <tag></tag>
   <elementGuidId>5605d648-2bbb-46f0-ba5e-4b03fc47bd8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@id, 'seasonDropdown')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>seasonDropdown</value>
   </webElementProperties>
</WebElementEntity>
