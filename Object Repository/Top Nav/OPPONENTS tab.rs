<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OPPONENTS tab</name>
   <tag></tag>
   <elementGuidId>d7cd591f-3ab2-47e6-a97f-a7dcceb8db83</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'my-opponents-nav-link']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>my-opponents-nav-link</value>
   </webElementProperties>
</WebElementEntity>
