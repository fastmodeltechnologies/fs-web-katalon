<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Toggle subject team of stat tile in ScoutBuilder</description>
   <name>Team Dropdown (Tile Editor)</name>
   <tag></tag>
   <elementGuidId>2221b877-bfe5-4cbb-b127-2f546d8059cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[1]/span/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[1]/span/span/span</value>
   </webElementProperties>
</WebElementEntity>
