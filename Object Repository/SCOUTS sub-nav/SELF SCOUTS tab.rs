<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SELF SCOUTS tab</name>
   <tag></tag>
   <elementGuidId>e482f587-605d-4a17-8453-0f6a5c69e573</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'self-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>self-1</value>
   </webElementProperties>
</WebElementEntity>
