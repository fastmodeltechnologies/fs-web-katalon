<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>ALL TEAMS button on the OPPONENTS page</description>
   <name>ALL TEAMS button</name>
   <tag></tag>
   <elementGuidId>01456a93-d1b6-4f83-831f-64d7431d2235</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'collegeFilter-all']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>collegeFilter-all</value>
   </webElementProperties>
</WebElementEntity>
