<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>SCHEDULED button on the OPPONENTS page</description>
   <name>SCHEDULED button</name>
   <tag></tag>
   <elementGuidId>e2e0d8a2-a58c-44ed-a019-9ad96725762e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'collegeFilter-opponents']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>collegeFilter-opponents</value>
   </webElementProperties>
</WebElementEntity>
