<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Div containing section header tile to verify the tile is on the page</description>
   <name>Depth Chart Tile (Presense Of)</name>
   <tag></tag>
   <elementGuidId>135ad025-5cb3-4b12-b3a8-2af039c79f5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-tile-type = 'depthChart']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tile-type</name>
      <type>Main</type>
      <value>depthChart</value>
   </webElementProperties>
</WebElementEntity>
