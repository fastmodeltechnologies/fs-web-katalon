<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Div containing section header tile to verify the tile is on the page</description>
   <name>Team Advanced Stats Tile (Presense Of)</name>
   <tag></tag>
   <elementGuidId>7328e6d6-7676-4f9f-a8b2-a333a2585e75</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-tile-type = 'teamAdvancedStatsTable']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tile-type</name>
      <type>Main</type>
      <value>teamAdvancedStatsTable</value>
   </webElementProperties>
</WebElementEntity>
