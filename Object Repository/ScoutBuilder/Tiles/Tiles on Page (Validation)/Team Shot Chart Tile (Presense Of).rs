<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Div containing section header tile to verify the tile is on the page</description>
   <name>Team Shot Chart Tile (Presense Of)</name>
   <tag></tag>
   <elementGuidId>e06f3261-0261-4e3c-9eab-d2cf50c19093</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-tile-type = 'teamShotChart']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tile-type</name>
      <type>Main</type>
      <value>teamShotChart</value>
   </webElementProperties>
</WebElementEntity>
