<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1st play listed in Plays menu</description>
   <name>Plays Tile 2</name>
   <tag></tag>
   <elementGuidId>5919e284-0dde-478a-b523-f2578b657517</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;playsTableRows&quot;]/tbody/tr[2]/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;playsTableRows&quot;]/tbody/tr[2]/td[2]</value>
   </webElementProperties>
</WebElementEntity>
