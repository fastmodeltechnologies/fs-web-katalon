<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Team Stats button on Tiles menu</description>
   <name>Team Stats (Tile Menu)</name>
   <tag></tag>
   <elementGuidId>fe5bb2de-a7f7-491d-a55f-e9766810043a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-team']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-team</value>
   </webElementProperties>
</WebElementEntity>
