<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click here to begin editing header of a tile in ScoutBuilder</description>
   <name>Tile Header Textbox</name>
   <tag></tag>
   <elementGuidId>8430b73c-1c75-42e7-902e-48f5d0ba774d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'scout-tile-title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>scout-tile-title</value>
   </webElementProperties>
</WebElementEntity>
