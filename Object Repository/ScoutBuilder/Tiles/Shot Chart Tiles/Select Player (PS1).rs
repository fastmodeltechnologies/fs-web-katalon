<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>player scout workflow select player</description>
   <name>Select Player (PS1)</name>
   <tag></tag>
   <elementGuidId>ecdb1973-d099-49b4-8112-32833e30d1dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div[1]/div[6]/main/div/div/div/div/div/div[2]/div[1]/div[2]/div/div/div/div[2]/div[2]/div/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div[1]/div[6]/main/div/div/div/div/div/div[2]/div[1]/div[2]/div/div/div/div[2]/div[2]/div/div/div/a</value>
   </webElementProperties>
</WebElementEntity>
