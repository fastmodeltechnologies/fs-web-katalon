<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Team Shot Chart Tile</name>
   <tag></tag>
   <elementGuidId>410eb501-84fc-45b7-b2a5-729dd9a09059</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;individualTileMenuOption-Team Chart&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;individualTileMenuOption-Team Chart&quot;]</value>
   </webElementProperties>
</WebElementEntity>
