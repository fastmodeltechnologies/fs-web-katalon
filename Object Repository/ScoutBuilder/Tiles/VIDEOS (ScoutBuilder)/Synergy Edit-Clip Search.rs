<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Synergy Edit-Clip Search</name>
   <tag></tag>
   <elementGuidId>cd26dd75-735d-4189-91d8-8cf996bb6b82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;search-icon-id&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;search-icon-id&quot;]/input</value>
   </webElementProperties>
</WebElementEntity>
