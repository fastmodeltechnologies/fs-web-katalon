<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Username field on Synergy login page</description>
   <name>Synergy Username</name>
   <tag></tag>
   <elementGuidId>2e14bc5c-48a6-4dbe-8be4-246030a04935</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'Username']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Username</value>
   </webElementProperties>
</WebElementEntity>
