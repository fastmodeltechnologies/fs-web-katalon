<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Password field on Synergy login page</description>
   <name>Synergy Password</name>
   <tag></tag>
   <elementGuidId>955b2a3e-7775-4caf-881e-a0d0fee4ac76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'Password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Password</value>
   </webElementProperties>
</WebElementEntity>
