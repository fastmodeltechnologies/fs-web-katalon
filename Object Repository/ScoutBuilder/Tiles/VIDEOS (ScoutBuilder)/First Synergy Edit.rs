<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First row in Synergy EDITS modal</description>
   <name>First Synergy Edit</name>
   <tag></tag>
   <elementGuidId>e43b0bc1-2145-4bc7-9445-5312cae03716</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;EditsTable&quot;]/tbody/tr[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;EditsTable&quot;]/tbody/tr[2]</value>
   </webElementProperties>
</WebElementEntity>
