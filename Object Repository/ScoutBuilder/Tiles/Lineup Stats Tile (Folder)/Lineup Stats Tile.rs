<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lineup Stats Tile</name>
   <tag></tag>
   <elementGuidId>eca31022-ceef-4766-8e9f-665c29a8b7a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='individualTileMenuOption-teamStatsLineup']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-teamStatsLineup']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-teamStatsLineup</value>
   </webElementProperties>
</WebElementEntity>
