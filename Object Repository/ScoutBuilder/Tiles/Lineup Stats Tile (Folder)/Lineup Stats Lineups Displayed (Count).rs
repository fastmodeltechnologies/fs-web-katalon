<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lineup Stats Lineups Displayed (Count)</name>
   <tag></tag>
   <elementGuidId>a625c590-642b-4d6a-b819-ba68ac65ee73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'lineupRowLimitInput']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lineupRowLimitInput</value>
   </webElementProperties>
</WebElementEntity>
