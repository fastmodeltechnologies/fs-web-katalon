<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lineup Stats Minimum Minutes</name>
   <tag></tag>
   <elementGuidId>ad0d1350-7129-45d7-a226-b548a6729b54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'lineupMinMinutesInput']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lineupMinMinutesInput</value>
   </webElementProperties>
</WebElementEntity>
