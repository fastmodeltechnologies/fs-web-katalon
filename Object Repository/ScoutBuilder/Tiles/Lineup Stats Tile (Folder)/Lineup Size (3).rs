<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Lineup Size 3 for Lineup Stats tile in ScoutBuilder</description>
   <name>Lineup Size (3)</name>
   <tag></tag>
   <elementGuidId>4f757ff6-4c51-4750-8f0d-99e6bb42aafe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'lineup-size-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lineup-size-3</value>
   </webElementProperties>
</WebElementEntity>
