<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FF Chart Dropdown TrueShootingPercentage</name>
   <tag></tag>
   <elementGuidId>7dd13c9e-cae1-4669-ba8d-f6127aaba62a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fourFactorChartStatDropdownOption-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fourFactorChartStatDropdownOption-1</value>
   </webElementProperties>
</WebElementEntity>
