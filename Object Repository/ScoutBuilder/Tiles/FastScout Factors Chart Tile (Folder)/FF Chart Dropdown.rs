<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Select Stat dropdown in Options tab for FastScout Factors Chart tile</description>
   <name>FF Chart Dropdown</name>
   <tag></tag>
   <elementGuidId>b5122947-609c-4362-9915-becb9dbb4516</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fourFactorChartStatDropdown']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fourFactorChartStatDropdown</value>
   </webElementProperties>
</WebElementEntity>
