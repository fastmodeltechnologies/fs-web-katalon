<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Minimum FGA Field (Top Scorers)</name>
   <tag></tag>
   <elementGuidId>3e80a311-d5f2-4a1b-b2de-4faf312cb238</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Minimum-FGA&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Minimum-FGA&quot;]/input</value>
   </webElementProperties>
</WebElementEntity>
