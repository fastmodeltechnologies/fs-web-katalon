<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Top Scorers Tile</name>
   <tag></tag>
   <elementGuidId>dff8e01e-a995-4102-9bfc-1a46fd71220f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;individualTileMenuOption-Top Scorers&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;individualTileMenuOption-Top Scorers&quot;]</value>
   </webElementProperties>
</WebElementEntity>
