<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BLK column header (on tile)</name>
   <tag></tag>
   <elementGuidId>69cd02e0-3578-47a2-95ac-ede4d5ecfa4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'inline-manual__BLK-header')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>inline-manual__BLK-header</value>
   </webElementProperties>
</WebElementEntity>
