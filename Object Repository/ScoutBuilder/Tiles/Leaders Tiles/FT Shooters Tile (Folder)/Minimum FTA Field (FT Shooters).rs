<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Minimum FTA Field (FT Shooters)</name>
   <tag></tag>
   <elementGuidId>79d0c3ec-94c0-46a1-aea2-65fd17090a87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Minimum-FTA&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Minimum-FTA&quot;]/input</value>
   </webElementProperties>
</WebElementEntity>
