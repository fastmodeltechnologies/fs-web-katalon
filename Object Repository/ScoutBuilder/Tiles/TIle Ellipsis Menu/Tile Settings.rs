<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Tile Settings button on ellipsis menu</description>
   <name>Tile Settings</name>
   <tag></tag>
   <elementGuidId>b5ed1c16-7293-4256-9ac7-603eb52c404a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'tileSettings']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tileSettings</value>
   </webElementProperties>
</WebElementEntity>
