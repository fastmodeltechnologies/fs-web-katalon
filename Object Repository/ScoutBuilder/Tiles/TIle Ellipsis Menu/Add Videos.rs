<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Add Videos option in Tile Ellipsis menu in ScoutBuilder</description>
   <name>Add Videos</name>
   <tag></tag>
   <elementGuidId>8e006863-31a4-42d3-ae8a-07b59142bd03</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'tileAddVideos']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tileAddVideos</value>
   </webElementProperties>
</WebElementEntity>
