<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tile SAMPLE tab</name>
   <tag></tag>
   <elementGuidId>254139a8-80c7-4389-9686-be2c42c374d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'menu-option-SAMPLE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-option-SAMPLE</value>
   </webElementProperties>
</WebElementEntity>
