<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>ENter text here to change tile header text in ScoutBuilder</description>
   <name>Tile Header Textbox (Input)</name>
   <tag></tag>
   <elementGuidId>f119f454-4768-4eac-bae5-0b82e1a30780</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'scout-tile-title-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>scout-tile-title-input</value>
   </webElementProperties>
</WebElementEntity>
