<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>2 x 2 custom table tile option when adding a Custom Table tile to a scout</description>
   <name>2x2 in tile builder</name>
   <tag></tag>
   <elementGuidId>dd403aea-becf-4a23-bab6-c85a147a36a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div[3]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div[3]/div[3]</value>
   </webElementProperties>
</WebElementEntity>
