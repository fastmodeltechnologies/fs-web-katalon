<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Checkbox for 'Date' column in Recent Games tile options menu</description>
   <name>Recent Games DATE column</name>
   <tag></tag>
   <elementGuidId>743391e0-fb1a-499b-9666-ddafdfec4c64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'column-option-date-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>column-option-date-label</value>
   </webElementProperties>
</WebElementEntity>
