<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Toggle to your team as the subject team for a stat tile in ScoutBuilder</description>
   <name>My Team (Team Dropdown) Tile Editor</name>
   <tag></tag>
   <elementGuidId>53deee50-ef0a-4fbe-857f-6e29f4d599b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'tile-team-option-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tile-team-option-1</value>
   </webElementProperties>
</WebElementEntity>
