<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Cumulative Boxscore Tile</name>
   <tag></tag>
   <elementGuidId>701dbc4d-abde-4788-94d4-337e849f5646</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-Cumulative Boxscore']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-Cumulative Boxscore</value>
   </webElementProperties>
</WebElementEntity>
