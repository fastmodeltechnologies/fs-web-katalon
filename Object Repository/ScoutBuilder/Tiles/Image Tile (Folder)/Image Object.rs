<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Used to upload images to image tile</description>
   <name>Image Object</name>
   <tag></tag>
   <elementGuidId>3b9c4005-ea9d-41cb-9f5b-a457c0e3fd90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@id, 'image-upload-input')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>image-upload-input</value>
   </webElementProperties>
</WebElementEntity>
