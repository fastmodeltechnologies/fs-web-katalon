<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Team Pace Chart Tile</name>
   <tag></tag>
   <elementGuidId>0bc1fa4a-6bd5-44c2-b8ee-0c78a8623620</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-Team Pace Chart']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-Team Pace Chart</value>
   </webElementProperties>
</WebElementEntity>
