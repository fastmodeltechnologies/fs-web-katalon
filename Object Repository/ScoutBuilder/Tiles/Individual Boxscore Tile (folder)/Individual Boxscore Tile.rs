<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Individual Boxscore Tile</name>
   <tag></tag>
   <elementGuidId>43b2a3af-4370-4b0f-8f19-5bfb2cae7a8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-Individual Boxscore']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-Individual Boxscore</value>
   </webElementProperties>
</WebElementEntity>
