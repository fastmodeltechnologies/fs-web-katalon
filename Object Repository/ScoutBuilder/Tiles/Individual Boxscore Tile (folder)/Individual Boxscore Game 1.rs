<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1st game listed on GAMES tab of Individual Boxscore tile</description>
   <name>Individual Boxscore Game 1</name>
   <tag></tag>
   <elementGuidId>7e161eb2-5bd3-4c54-abab-e9aef859037e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;input-0&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;input-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
