<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>All Games offensive split (Team Stat Splits tile)</name>
   <tag></tag>
   <elementGuidId>60654d92-8cb3-4c94-91c6-f7d68acddb5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'split-regularseason']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>split-regularseason</value>
   </webElementProperties>
</WebElementEntity>
