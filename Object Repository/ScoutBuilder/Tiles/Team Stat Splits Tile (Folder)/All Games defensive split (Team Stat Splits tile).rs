<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>All Games defensive split (Team Stat Splits tile)</name>
   <tag></tag>
   <elementGuidId>d79a0056-ca1e-47f8-b06f-2d1a279ccc99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'split-def-regularseason']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>split-def-regularseason</value>
   </webElementProperties>
</WebElementEntity>
