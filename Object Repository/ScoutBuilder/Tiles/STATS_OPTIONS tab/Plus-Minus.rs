<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Checkbox for +/- stat</description>
   <name>Plus-Minus</name>
   <tag></tag>
   <elementGuidId>ba96910c-e995-444c-8d97-5d51369fc9ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkbox-+/--0-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox-+/--0-label</value>
   </webElementProperties>
</WebElementEntity>
