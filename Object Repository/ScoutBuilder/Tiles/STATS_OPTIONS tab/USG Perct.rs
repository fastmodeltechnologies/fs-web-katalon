<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Checkbox for the USG% stat</description>
   <name>USG Perct</name>
   <tag></tag>
   <elementGuidId>336705f1-914d-486f-9027-310af80d206f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkbox-Usg%-0-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox-Usg%-0-label</value>
   </webElementProperties>
</WebElementEntity>
