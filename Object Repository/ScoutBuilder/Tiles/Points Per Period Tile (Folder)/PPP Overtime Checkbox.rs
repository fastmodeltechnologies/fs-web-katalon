<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PPP Overtime Checkbox</name>
   <tag></tag>
   <elementGuidId>becadac2-fd9e-483e-aa01-a8367fcf07aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'showOvertimeCheckbox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>showOvertimeCheckbox</value>
   </webElementProperties>
</WebElementEntity>
