<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>ADD... button for CUSTOM SPLITS. Applies to Personnel, Team Stat Splits tiles</description>
   <name>Add Custom Split</name>
   <tag></tag>
   <elementGuidId>16901e39-49a6-47f0-b9a9-1375e0eb63c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-custom-split']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-custom-split</value>
   </webElementProperties>
</WebElementEntity>
