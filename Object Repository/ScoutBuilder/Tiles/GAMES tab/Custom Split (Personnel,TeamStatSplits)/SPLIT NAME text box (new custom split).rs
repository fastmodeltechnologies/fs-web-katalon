<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Custom split name</description>
   <name>SPLIT NAME text box (new custom split)</name>
   <tag></tag>
   <elementGuidId>5e78a358-88ad-4799-a1d6-31872f4ef68c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'split-name-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>split-name-input</value>
   </webElementProperties>
</WebElementEntity>
