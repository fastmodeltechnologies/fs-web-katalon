<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>ADD button in NEW CUSTOM SPLIT modal</description>
   <name>ADD new custom split button</name>
   <tag></tag>
   <elementGuidId>19f9e4ae-a112-41ce-8993-410453b389cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addSplit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addSplit</value>
   </webElementProperties>
</WebElementEntity>
