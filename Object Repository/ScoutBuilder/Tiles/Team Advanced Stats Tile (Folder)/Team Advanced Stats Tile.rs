<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Team Advanced Stats Tile</name>
   <tag></tag>
   <elementGuidId>4c619e07-06aa-48fb-9a38-42823daabbc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-teamAdvancedStatsTable']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-teamAdvancedStatsTable</value>
   </webElementProperties>
</WebElementEntity>
