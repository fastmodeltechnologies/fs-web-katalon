<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FastClean theme</name>
   <tag></tag>
   <elementGuidId>31865520-dd9e-4dbe-a8d8-dd73142e7002</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'theme-option-FAST_AND_CLEAN_THEME']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>theme-option-FAST_AND_CLEAN_THEME</value>
   </webElementProperties>
</WebElementEntity>
