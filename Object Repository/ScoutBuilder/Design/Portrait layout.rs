<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Portrait layout</name>
   <tag></tag>
   <elementGuidId>ba5e2def-0c3b-4ea0-a8f7-dc1042768b05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'layout-option-PORTRAIT']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>layout-option-PORTRAIT</value>
   </webElementProperties>
</WebElementEntity>
