<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Opponent Logo</name>
   <tag></tag>
   <elementGuidId>5354c4fd-4b2d-4458-ae2b-d3f6e38b6cbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'logo-option-OPPONENT_TEAM']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>logo-option-OPPONENT_TEAM</value>
   </webElementProperties>
</WebElementEntity>
