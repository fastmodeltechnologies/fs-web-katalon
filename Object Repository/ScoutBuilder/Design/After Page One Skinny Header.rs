<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>After Page One Skinny Header</name>
   <tag></tag>
   <elementGuidId>67c1064b-5f4a-4822-8fff-dca27698d202</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'header-size-option-CONDENSED']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>header-size-option-CONDENSED</value>
   </webElementProperties>
</WebElementEntity>
