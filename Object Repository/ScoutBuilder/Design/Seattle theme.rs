<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Seattle theme</name>
   <tag></tag>
   <elementGuidId>a73e6c0b-bda2-4c36-95b5-f27f4f4a7543</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'theme-option-SEATTLE_THEME']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>theme-option-SEATTLE_THEME</value>
   </webElementProperties>
</WebElementEntity>
