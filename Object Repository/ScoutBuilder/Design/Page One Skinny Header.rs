<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page One Skinny Header</name>
   <tag></tag>
   <elementGuidId>0dab3f3d-a55a-47b7-ad85-94005e51a4e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'first-header-size-option-CONDENSED']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>first-header-size-option-CONDENSED</value>
   </webElementProperties>
</WebElementEntity>
