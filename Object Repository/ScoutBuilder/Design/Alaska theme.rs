<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Alaska theme</name>
   <tag></tag>
   <elementGuidId>e8a4cfec-0d11-491f-be84-0c10f1fdc9a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'theme-option-ALASKA_THEME']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>theme-option-ALASKA_THEME</value>
   </webElementProperties>
</WebElementEntity>
