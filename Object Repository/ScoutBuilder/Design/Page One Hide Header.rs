<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page One Hide Header</name>
   <tag></tag>
   <elementGuidId>8da2e247-784e-4603-a2c7-467a2bf42604</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'first-header-size-option-NONE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>first-header-size-option-NONE</value>
   </webElementProperties>
</WebElementEntity>
