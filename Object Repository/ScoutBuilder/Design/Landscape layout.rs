<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Landscape layout</name>
   <tag></tag>
   <elementGuidId>e9e0d3c8-cc94-496f-bccb-343d91a33d2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'layout-option-LANDSCAPE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>layout-option-LANDSCAPE</value>
   </webElementProperties>
</WebElementEntity>
