<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Standard theme</name>
   <tag></tag>
   <elementGuidId>64c2e1f4-c46d-4738-b2e0-76ed13179646</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'theme-option-STANDARD_THEME']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>theme-option-STANDARD_THEME</value>
   </webElementProperties>
</WebElementEntity>
