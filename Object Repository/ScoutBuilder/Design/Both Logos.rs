<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Both Logos</name>
   <tag></tag>
   <elementGuidId>415379c2-dc6e-42d4-a697-155fb1239867</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'logo-option-BOTH_TEAMS']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>logo-option-BOTH_TEAMS</value>
   </webElementProperties>
</WebElementEntity>
