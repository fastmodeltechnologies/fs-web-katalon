<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Button to add page along right-hand pagination menu</description>
   <name>Add Page button</name>
   <tag></tag>
   <elementGuidId>c41b8aa1-5ef1-4d88-b2a2-f23346c3fa7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;add-report-page-button&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;add-report-page-button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
