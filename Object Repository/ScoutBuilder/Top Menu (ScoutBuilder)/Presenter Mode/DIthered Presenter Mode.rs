<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>dithered presenter mode button for users without feature</description>
   <name>Dithered Presenter Mode</name>
   <tag></tag>
   <elementGuidId>8f68044a-8d77-4edd-bc2f-f41655a4b0b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[1]/div[1]/div/div[2]/span[2][count(. | //*[@class = 'cursor-not-allowed']) = count(//*[@class = 'cursor-not-allowed'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[1]/div[1]/div/div[2]/span[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cursor-not-allowed</value>
   </webElementProperties>
</WebElementEntity>
