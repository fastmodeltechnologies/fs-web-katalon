<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Save button in Create New Template modal</description>
   <name>Save new template button</name>
   <tag></tag>
   <elementGuidId>849b60c2-b2ce-451f-89f1-8a700929cab2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'save-template-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>save-template-button</value>
   </webElementProperties>
</WebElementEntity>
