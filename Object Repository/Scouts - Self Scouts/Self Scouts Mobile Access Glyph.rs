<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Self Scouts Mobile Access Glyph</name>
   <tag></tag>
   <elementGuidId>cdfb1364-7da6-4c05-be8b-756e5d681a46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]/td[8]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]/td[8]/div/span</value>
   </webElementProperties>
</WebElementEntity>
