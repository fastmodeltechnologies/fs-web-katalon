<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Scout Context Menu (Self Scouts)</name>
   <tag></tag>
   <elementGuidId>d69ac2b8-36e3-4457-bf49-b5efd7cf4fff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]/td[9]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]/td[9]/div</value>
   </webElementProperties>
</WebElementEntity>
