<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Top scout listed on SCOUTS>SELF SCOUTS page</description>
   <name>Self Scouts 1st Row</name>
   <tag></tag>
   <elementGuidId>b6e3e17e-2449-4848-b745-aeee6ab77b51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;selfScoutTable&quot;]/tbody/tr[2]</value>
   </webElementProperties>
</WebElementEntity>
