<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Scout Archive Menu - Consumer</name>
   <tag></tag>
   <elementGuidId>1e8a74fb-c60e-4284-8a45-f3a67087e7d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]/td[7]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]/td[7]/div</value>
   </webElementProperties>
</WebElementEntity>
