<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>button to delete scouting reports</description>
   <name>Delete button</name>
   <tag></tag>
   <elementGuidId>8a32b66b-be12-482c-abef-1eee53608892</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'delete']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>delete</value>
   </webElementProperties>
</WebElementEntity>
