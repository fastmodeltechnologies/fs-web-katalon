<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>first player listed on Roster page to support workflow</description>
   <name>1st player on Roster page</name>
   <tag></tag>
   <elementGuidId>62d83daa-efae-48a4-a57b-4c91eb5c67a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div/div/div[2]/div/table/tbody/tr[1]/td/div/div/div[2]/div[1]/div/div/div/a/span/span/span[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div/div/div[2]/div/table/tbody/tr[1]/td/div/div/div[2]/div[1]/div/div/div/a/span/span/span[2]/span</value>
   </webElementProperties>
</WebElementEntity>
