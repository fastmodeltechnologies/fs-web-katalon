<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Advanced Stats table (Dashboard)</name>
   <tag></tag>
   <elementGuidId>3eaeddfb-04bd-4ef6-a3d2-2ed177df7629</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'advanced-stats-title-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>advanced-stats-title-id</value>
   </webElementProperties>
</WebElementEntity>
