<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Fastscout Factor Chart Object (Dashboard)</name>
   <tag></tag>
   <elementGuidId>2c500c20-785e-45d1-999c-4222675b4e05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fourFactorsChart']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fourFactorsChart</value>
   </webElementProperties>
</WebElementEntity>
