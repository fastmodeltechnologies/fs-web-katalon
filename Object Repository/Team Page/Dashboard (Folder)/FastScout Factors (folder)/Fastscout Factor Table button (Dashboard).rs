<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Fastscout Factor Table button (Dashboard)</name>
   <tag></tag>
   <elementGuidId>d7f9dbff-ed27-4a39-9935-23046c463dc1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fourFactorsTableIcon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fourFactorsTableIcon</value>
   </webElementProperties>
</WebElementEntity>
