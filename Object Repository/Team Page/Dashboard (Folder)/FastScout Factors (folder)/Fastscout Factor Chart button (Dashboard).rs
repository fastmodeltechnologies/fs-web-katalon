<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Fastscout Factor Chart button (Dashboard)</name>
   <tag></tag>
   <elementGuidId>fd247f83-b466-46bb-9cd2-b4329808f51d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fourFactorsChartIcon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fourFactorsChartIcon</value>
   </webElementProperties>
</WebElementEntity>
