<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TOTALS boxscore button</name>
   <tag></tag>
   <elementGuidId>e1afaa48-2fdf-4066-a291-aa408b07d720</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'current-season-filter-totals-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>current-season-filter-totals-id</value>
   </webElementProperties>
</WebElementEntity>
