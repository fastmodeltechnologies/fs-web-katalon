<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Confirming the button is not available for users without AdvancedStats feature</description>
   <name>Per 40 boxscore button (not available)</name>
   <tag></tag>
   <elementGuidId>05e80b5d-b40b-473b-a72c-9f6761123998</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;team-record-title-id&quot;]/div/span[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;team-record-title-id&quot;]/div/span[4]</value>
   </webElementProperties>
</WebElementEntity>
