<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ADVANCED boxscore button</name>
   <tag></tag>
   <elementGuidId>72c0afb8-4bec-424b-9381-00800d460bca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'current-season-filter-advanced-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>current-season-filter-advanced-id</value>
   </webElementProperties>
</WebElementEntity>
