<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AVERAGES boxscore button</name>
   <tag></tag>
   <elementGuidId>076759f9-7c98-4e96-8620-e60ca48336d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'current-season-filter-normal-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>current-season-filter-normal-id</value>
   </webElementProperties>
</WebElementEntity>
