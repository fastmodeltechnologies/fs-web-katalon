<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PlusMinus column header</name>
   <tag></tag>
   <elementGuidId>857c759b-0b7d-4afd-beca-ba7175643462</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@col = '+/-']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>col</name>
      <type>Main</type>
      <value>+/-</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;advancedBoxscore&quot;]/thead/tr/th[15]/div/div</value>
   </webElementProperties>
</WebElementEntity>
