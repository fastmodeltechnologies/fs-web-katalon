<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select All (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>d9974afc-377f-459f-a0a4-070db8a92e9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'select-all-opponent-schedule']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>select-all-opponent-schedule</value>
   </webElementProperties>
</WebElementEntity>
