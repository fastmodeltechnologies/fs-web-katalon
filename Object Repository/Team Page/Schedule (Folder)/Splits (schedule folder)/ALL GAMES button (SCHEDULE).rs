<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ALL GAMES button (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>1abaa145-24fb-4dfd-b8e2-d28bb35cbc22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'opponent-schedule-SELECT_ALL']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>opponent-schedule-SELECT_ALL</value>
   </webElementProperties>
</WebElementEntity>
