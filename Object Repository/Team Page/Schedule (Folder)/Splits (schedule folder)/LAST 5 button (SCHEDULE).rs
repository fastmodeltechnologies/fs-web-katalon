<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LAST 5 button (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>c7d9f9e2-2ded-4c5e-91db-b543697cf2ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'opponent-schedule-LAST_N']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>opponent-schedule-LAST_N</value>
   </webElementProperties>
</WebElementEntity>
