<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CONFERENCE button (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>8076636d-b93b-49f8-9d58-c9cb7ae1fe4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'opponent-schedule-CONFERENCE_GAMES']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>opponent-schedule-CONFERENCE_GAMES</value>
   </webElementProperties>
</WebElementEntity>
