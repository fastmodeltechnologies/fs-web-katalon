<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HOME button (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>9dc2e0d5-8860-44cc-a0b7-4615a1f4340a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'opponent-schedule-HOME_GAMES']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>opponent-schedule-HOME_GAMES</value>
   </webElementProperties>
</WebElementEntity>
