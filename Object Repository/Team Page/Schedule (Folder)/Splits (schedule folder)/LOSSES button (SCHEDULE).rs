<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LOSSES button (SCHEDULE)</name>
   <tag></tag>
   <elementGuidId>b31ee6c9-7978-4a0b-92af-163b3b1dd5a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'opponent-schedule-LOSSES']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>opponent-schedule-LOSSES</value>
   </webElementProperties>
</WebElementEntity>
